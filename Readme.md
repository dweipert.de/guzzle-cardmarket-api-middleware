# guzzle-cardmarket-api-middleware

to use with the [Cardmarket API](https://api.cardmarket.com/ws/documentation)

## Installation
```
composer require dweipert/guzzle-cardmarket-api-middleware
```

## How to use

```php
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Dweipert\CardmarketApi\OAuthMiddleware;

// Create default HandlerStack
$stack = HandlerStack::create();

// Add this middleware to the top with `push`
$stack->push(new OAuthMiddleware(), 'oauth');

// Initialize the client with the handler option and cardmarket oauth data
$client = new Client([
    'handler' => $stack,
    'base_uri' => 'https://api.cardmarket.com/ws/v2.0/output.json/',
    'cardmarket' => [
        'app_token' => 'your_app_token',
        'app_secret' => 'your_app_secret',
        'access_token' => 'your_access_token',
        'access_token_secret' => 'your_access_token_secret',
    ],
]);

// Request a resource
$response = $client->get('account');
```
