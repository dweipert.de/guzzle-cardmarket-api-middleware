<?php

namespace Dweipert\CardmarketApi\Tests;

use Dweipert\CardmarketApi\OAuthMiddleware;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use PHPUnit\Framework\TestCase;

class Test extends TestCase
{
    private Client $client;

    public function setUp(): void
    {
        // Create default HandlerStack
        $stack = HandlerStack::create();

        // Add this middleware to the top with `push`
        $stack->push(new OAuthMiddleware(), 'oauth');

        // Initialize the client with the handler option and cardmarket oauth data
        $this->client = new Client([
            'handler' => $stack,
            'http_errors' => false,
            'base_uri' => 'https://sandbox.cardmarket.com/ws/v2.0/output.json/',
            'cardmarket' => [
                'app_token' => $_ENV['CARDMARKET_APP_TOKEN'],
                'app_secret' => $_ENV['CARDMARKET_APP_SECRET'],
                'access_token' => $_ENV['CARDMARKET_ACCESS_TOKEN'],
                'access_token_secret' => $_ENV['CARDMARKET_ACCESS_TOKEN_SECRET'],
            ],
        ]);
    }

    public function testOAuthSuccess()
    {
        $response = $this->client->get('account');

        $this->assertEquals(200, $response->getStatusCode());
    }
}
